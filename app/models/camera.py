from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .user import User  # noqa: F401


# class Camera(Base):
#     id = Column(Integer, primary_key=True, index=True)
#     title = Column(String, index=True)
#     description = Column(String, index=True)
#
#     url = Column(String, index=False)
#     sub_url = Column(String, index=False)
#     slug = Column(String, unique=True)
#     schedule = Column(String, index=True)
#
#     owner_id = Column(Integer, ForeignKey("user.id"))
#     owner = relationship("User", back_populates="cameras")
