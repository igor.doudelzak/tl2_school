from sqlalchemy import Boolean

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from db.base_class import Base
# if TYPE_CHECKING:
#     from .user import User  # noqa: F401

# Create a base class for declarative class definitions
# Base = declarative_base()


# Define your database models as subclasses of Base
class Camera(Base):
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)

    url = Column(String, index=False)
    sub_url = Column(String, index=False)
    slug = Column(String, unique=True)
    schedule = Column(String, index=True)

    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="cameras")


class User(Base):
    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    cameras = relationship("Camera", back_populates="owner")
