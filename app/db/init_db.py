import logging

from sqlalchemy.orm import Session

from app import crud, schemas
from app.core.config import settings
from app.db import base  # noqa: F401

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


persp01 = {
    "title": "Perspectivniy",
    "slug": "persp01",
    "description": "Some descr",
    "owner_id": 1,
    "url": "rtsp://admin:Ad-654321@78.25.93.138:5554/cam/realmonitor?channel=1&subtype=0",
    "schedule": None
}

persp02 = {
    "title": "Perspectivniy 2",
    "slug": "persp02",
    "description": "Some descr",
    "owner_id": 1,
    "sub_url": "rtsp://admin:Ad-654321@78.25.93.154:5541/cam/realmonitor?channel=1&subtype=1",
    "url": "rtsp://admin:Ad-654321@78.25.93.154:5541/cam/realmonitor?channel=1&subtype=0",
    "schedule": None
}

cameras = [persp01, persp02]

def init_db(db: Session) -> None:
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    # Base.metadata.create_all(bind=engine)

    user = crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER)
    if not user:
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPERUSER,
            password=settings.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
        )
        user = crud.user.create(db, obj_in=user_in)  # noqa: F841

    for cam in cameras:
        cam_db = crud.camera.get_by_slug(db, slug=cam['slug'])
        if not cam_db:
            cam_for_db = schemas.CameraUpdate(
                **cam
            )
            camera = crud.camera.create(db, obj_in=cam_for_db)
            logging.info(f"Create Camera {camera.title} ")
        else:
            logging.info(f"Camera {cam_db.title} exists !!")
