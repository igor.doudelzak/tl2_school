# Import all the models, so that Base has them before being
# imported by Alembic
from db.base_class import Base  # noqa
from models.models import User  # noqa
from models.models import Camera  # noqa
