from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# from core.config import settings

engine = create_engine("sqlite:////home/oleg/PycharmProjects/tl2/tl2.db")
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
