import asyncio
import logging
import os
from telebot import types
from telebot.async_telebot import AsyncTeleBot
from dotenv import load_dotenv
import sqlite3
import cv2
from datetime import date
from datetime import datetime

load_dotenv()
BOT_TOKEN = os.getenv("BOT_TOKEN")
bot = AsyncTeleBot(BOT_TOKEN)
project_path = os.getenv("PROJECT_PATH")


async def capture_frame_from_rtsp(rtsp_url):
    cap = cv2.VideoCapture(rtsp_url)
    if not cap.isOpened():
        print("Error: Unable to open RTSP stream")
        return None

    ret, frame = cap.read()
    cap.release()

    if not ret:
        print("Error: Unable to read frame from RTSP stream")
        return None
    return frame


async def save_the_image(camera_slug, image):
    dt = date.today().strftime("%y%m%d")
    tm = datetime.now().strftime("%H%M%S")
    s_path = os.path.join(project_path, "images", camera_slug)
    if not os.path.exists(s_path):
        os.makedirs(s_path, 0o775)
    image_path = os.path.join(s_path, dt + '-' + tm + '-' + camera_slug + ".jpg")
    cv2.imwrite(image_path, image)
    return image_path


@bot.message_handler(commands=['start', 'hi', 'cams'])
async def command_handler(message):
    if message.text == "/start":
        await bot.send_message(message.chat.id, f"-->>  Пользуйтесь командами \n/cams - работа с камерами\n"
                                                f"/now - фото со всех камер")
    if message.text == "/hi":
        await bot.reply_to(message, "Hello World!")
    if message.text == "/cams":
        conn = sqlite3.connect("tl2.db")
        cursor = conn.cursor()
        for row in cursor.execute('SELECT url, slug FROM camera'):
            logging.warning(f"{row=}")
            camera_url = row[0]
            camera_slug = row[1]
            try:
                image = await capture_frame_from_rtsp(camera_url)
                imfl = await save_the_image(camera_slug, image)

                image_to_send = open(imfl, 'rb')
                await bot.send_photo(message.chat.id, image_to_send, caption=camera_slug)
            except cv2.error as e:
                await bot.send_message(message.chat.id, f"Камера {camera_slug} недоступна")
        cv2.destroyAllWindows()
        conn.close()


async def start_async_bot():
    await bot.infinity_polling()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    asyncio.run(start_async_bot())
