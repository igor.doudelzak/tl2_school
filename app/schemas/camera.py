from typing import Optional

from pydantic import BaseModel


# Shared properties
class CameraBase(BaseModel):
    title: Optional[str] = None
    slug: Optional[str]


# Properties to receive on camera creation
class CameraCreate(CameraBase):
    title: str
    slug: Optional[str]


# Properties to receive on camera update
class CameraUpdate(CameraCreate):
    id: Optional[int]
    title: Optional[str]
    description: Optional[str]
    owner_id: Optional[int]
    url: Optional[str]
    schedule: Optional[str]


# Properties shared by models stored in DB
class CameraInDBBase(CameraBase):
    id: int
    title: Optional[str]
    slug: Optional[str]
    description: Optional[str] = None
    owner_id: Optional[int]
    url: Optional[str]
    schedule: Optional[str]

    class Config:
        from_attributes = True


# Properties to return to client
class Camera(CameraInDBBase):
    pass


# Properties properties stored in DB
class CameraInDB(CameraInDBBase):
    pass
