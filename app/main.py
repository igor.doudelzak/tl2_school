import crud
import schemas
from sqlalchemy.orm import Session
from db import base  # noqa: F401
from db.session import SessionLocal
import sqlite3
import logging


def start(camera_slug, db: Session):
    cam_db = crud.camera.get_by_slug(db, slug=camera_slug)
    if cam_db:
        print(f"Camera found: {cam_db}")
    else:
        print("Camera not found.")


if __name__ == "__main__":
    conn = sqlite3.connect("tl2.db")
    cursor = conn.cursor()
    for row in cursor.execute('SELECT * FROM camera'):
        logging.warning(row)
    conn.close()
    # db = SessionLocal()
    #
    # try:
    #     # Call the start function with the initialized db session
    #     start("persp01", db)
    # finally:
    #     # Ensure the session is closed after use
    #     db.close()
