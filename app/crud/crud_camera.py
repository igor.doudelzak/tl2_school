from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from crud.base import CRUDBase
from models.models import Camera
from schemas.camera import CameraCreate, CameraUpdate


class CRUDCamera(CRUDBase[Camera, CameraCreate, CameraUpdate]):
    def get_by_slug(self, db: Session, *, slug: str) -> Optional[CameraUpdate]:
        return db.query(Camera).filter(Camera.slug == slug).first()

    def create_with_owner(
        self, db: Session, *, obj_in: CameraCreate, owner_id: int
    ) -> Camera:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Camera]:
        return (
            db.query(self.model)
            .filter(Camera.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )


camera = CRUDCamera(Camera)
